package com.mastertech.cartao.models.DTO;

public class CartaoDTO {
    public boolean ativo;

    public CartaoDTO() {
    }

    public CartaoDTO(boolean ativo) {
        this.ativo = ativo;
    }
    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }


}
