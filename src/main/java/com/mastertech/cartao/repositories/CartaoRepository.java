package com.mastertech.cartao.repositories;

import com.mastertech.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface CartaoRepository extends CrudRepository<Cartao,Integer> {
    Optional<Cartao> findByNumero(String numero);
}
