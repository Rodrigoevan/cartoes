package com.mastertech.cartao.repositories;

import com.mastertech.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento,Integer> {
    Iterable<Pagamento> findByCartaoId(Integer cartaoId);
}
