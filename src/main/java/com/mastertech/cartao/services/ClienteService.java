package com.mastertech.cartao.services;

import com.mastertech.cartao.models.Cliente;
import com.mastertech.cartao.repositories.ClienteRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Optional<Cliente> buscarPorId(Integer id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return  clienteOptional;
    }
}
