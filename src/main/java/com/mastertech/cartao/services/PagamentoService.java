package com.mastertech.cartao.services;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.Pagamento;
import com.mastertech.cartao.repositories.CartaoRepository;
import com.mastertech.cartao.repositories.PagamentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;

@Service
public class PagamentoService {
    private static final String CARTAO_NAO_ENCONTRADO = "Cartão não encontrado";

    @Autowired
    PagamentoRepository pagamentoRepository;
    @Autowired
    CartaoRepository cartaoRepository;

    public Pagamento efetuarCompra(Pagamento pagamento){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(pagamento.getCartaoId());
        if(cartaoOptional.isPresent()){
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }
        else {
            throw new ObjectNotFoundException(Cartao.class, CARTAO_NAO_ENCONTRADO);
        }
    }


    public Iterable<Pagamento> buscarExtrato(Integer cartaoId) {
        Iterable<Pagamento> extrato = pagamentoRepository.findByCartaoId(cartaoId);
        return extrato;
    }
}
