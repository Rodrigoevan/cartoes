package com.mastertech.cartao.services;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.Cliente;
import com.mastertech.cartao.repositories.CartaoRepository;
import com.mastertech.cartao.repositories.ClienteRepository;
import javassist.NotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    private static final String CLIENTE_NAO_ENCONTRADO = "Cliente Não encontrado";
    private static final String CARTAO_NAO_ENCONTRADO = "Cartão não encontrado";
    private static final String ESTE_CARTAO_JA_EXISTE = "Este cartão já existe";
    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    public Cartao inserirCartao(Cartao cartao) throws ObjectNotFoundException {
        Optional<Cliente> clienteObjeto = clienteRepository.findById(cartao.getClienteId());
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(cartao.getNumero());
        if(cartaoOptional.isPresent()){
            throw new RuntimeException(ESTE_CARTAO_JA_EXISTE);
        }
        if(clienteObjeto.isPresent()){
            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return  cartaoObjeto;
        }else {
            throw new ObjectNotFoundException(Cartao.class,CLIENTE_NAO_ENCONTRADO);
        }

    }
    public Cartao ativarCartao(String numero, boolean status){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            cartaoOptional.get().setAtivo(status);
            cartaoRepository.save(cartaoOptional.get());
            return cartaoOptional.get();
        }else{
            throw new ObjectNotFoundException(Cartao.class,CARTAO_NAO_ENCONTRADO);
        }

    }

    public Cartao buscarCartaoPorNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }else{
            throw new ObjectNotFoundException(Cartao.class,CARTAO_NAO_ENCONTRADO);
        }

    }
}
