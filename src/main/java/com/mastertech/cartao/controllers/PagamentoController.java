package com.mastertech.cartao.controllers;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.Pagamento;
import com.mastertech.cartao.services.PagamentoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.security.x509.OtherName;

import java.io.StringWriter;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
    private static final String CARTAO_NAO_ENCONTRADO = "Cartão não encontrado";

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> efetuarPagamento(@RequestBody Pagamento pagamento){
        try {
            Pagamento pagamentoObjeto = pagamentoService.efetuarCompra(pagamento);
            return ResponseEntity.status(201).body(pagamentoObjeto);
        }
        catch (Exception e){
            throw new ObjectNotFoundException(Pagamento.class,CARTAO_NAO_ENCONTRADO);
        }
    }
    @GetMapping("/{cartao_id}")
    public Iterable<Pagamento> exibirPagamentos(@PathVariable Integer cartao_id){
        Iterable<Pagamento> pagamentosIterable = pagamentoService.buscarExtrato(cartao_id);
        return pagamentosIterable;
    }
}
