package com.mastertech.cartao.controllers;

import com.mastertech.cartao.models.Cliente;
import com.mastertech.cartao.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> inserirCliente(@RequestBody Cliente cliente){
        Cliente clienteObjeto = clienteService.criarCliente(cliente);
        return ResponseEntity.status(201).body(cliente);
    }
    @GetMapping("/{id}")
    public Cliente buscarClientePorId(@PathVariable Integer id){
        Optional<Cliente> clienteOptional = clienteService.buscarPorId(id);
        if (clienteOptional.isPresent()){
            return clienteOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }

    }
}
