package com.mastertech.cartao.controllers;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.DTO.CartaoDTO;
import com.mastertech.cartao.services.CartaoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
    private static final String CLIENTE_NAO_ENCONTRADO = "Não é possivel criar um cartão de um cliente que não exite";
    private static final String CARTAO_NAO_ENCONTRADO = "Cartão não encontrado";
    private static final String ESTE_CARTAO_JA_EXISTE = "Este cartão já existe";
    @Autowired
    CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<Cartao> inserirCartao(@RequestBody Cartao cartao){
        try{
            Cartao cartaoObjeto = cartaoService.inserirCartao(cartao);
            return ResponseEntity.status(201).body(cartaoObjeto);
        }catch (Exception e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @PatchMapping("/{numero}")
    public ResponseEntity ativarCartao(@PathVariable String numero, @RequestBody CartaoDTO cartaoDTO){
        Cartao cartaoObjeto = cartaoService.ativarCartao(numero,cartaoDTO.isAtivo());
        return ResponseEntity.status(200).body(cartaoObjeto);
    }
    @GetMapping("/{numero}")
    public ResponseEntity buscarCartao(@PathVariable String numero){
        try {
            Cartao cartaoObjeto = cartaoService.buscarCartaoPorNumero(numero);
            return ResponseEntity.status(200).body(cartaoObjeto);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, CARTAO_NAO_ENCONTRADO);
        }

    }
}
